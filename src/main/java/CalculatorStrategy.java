public interface CalculatorStrategy {

    public int CalculateOperation (int num1 ,int num2);
}
