public class ManageCalculatorStrategy {
    // or contextmanager
    CalculatorStrategy strategy ;

    public void setStrategy(CalculatorStrategy strategy) {
        this.strategy = strategy;
    }

    public int executeCalculatorStrategy(int num1, int num2) {
        return strategy.CalculateOperation(num1 , num2);
    }
}
