import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        CalculatorStrategy strategy =null ;
        ManageCalculatorStrategy ctx =new ManageCalculatorStrategy();

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter 2 number to perform operation on:");
        int num1 =sc.nextInt();
        int num2 =sc.nextInt();

        System.out.println("Enter the operation to be executed : 1->+ ,2-> - , 3->* , 4->/:");
        int op = sc.nextInt();
        switch (op) {

            case 1:
                ctx.setStrategy(new StrategyAdd());
                break;

            case 2:
                ctx.setStrategy(new StrategySubtract());
                break;
            case 3:
                ctx.setStrategy(new StrategyMultiply());
                break;
            case 4 :
                ctx.setStrategy(new StrategyDivide());
                break;
            default:
                System.out.println("invalid input ");
                break;
        }

        ctx.executeCalculatorStrategy(num1,num2);
    }
}
